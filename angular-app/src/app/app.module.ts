// import { BrowserModule } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';
//
// import { AppRoutingModule } from './app-routing.module';
// import { AppComponent } from './app.component';
// import { UserComponent } from "./components/user/user.component";
// import { HelloComponent } from "./components/hello/hello.component";
// import {TravelComponent} from "./components/travel/travel.component";
// import {WarningalertComponent} from "./components/warningalert/warningalert.component";
// import { CodeboundComponent } from './components/codebound/codebound.component';
// import { MathComponent } from './components/math/math.component';
// import { VipComponent } from './components/vip/vip.component';
//
//
// @NgModule({
//   declarations: [
//     AppComponent,
//     UserComponent,
//     HelloComponent,
//     TravelComponent,
//     WarningalertComponent,
//     CodeboundComponent,
//     MathComponent,
//     VipComponent
//   ],
//   imports: [
//     BrowserModule,
//     AppRoutingModule
//   ],
//   providers: [],
//   bootstrap: [AppComponent]
// })
// export class AppModule { }





//TEMPLATE FORM

//SIMPLE FORMS EXAMPLE

import { Component } from '@angular/core';
// import {BrowserModule} from "@angular/platform-browser";
// import {NgModule} from '@angular/core';
// import {FormsModule} from "@angular/forms";
// import {AppComponent} from './app.component'
//
// @NgModule({
//   declarations: [AppComponent
//   ],imports: [
//     BrowserModule,
//     FormsModule
//   ],
//   providers:[],
//   bootstrap: [AppComponent]
//
// })
// export class AppModule {}

//REACTIVE FORMS EXAMPLE
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';



// import {FontAwesomeModule} from '@fontawesome/angular-fontawesome';
import {BrowserModule} from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";

//COMPONENTS
import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { VipComponent } from "./components/vip/vip.component";
import { PostsComponent } from './components/posts/posts.component';

//SERVICES
import { DataService } from './services/data.service';
import { PostsService } from "./services/posts.service";
import { PostFormComponent } from './components/post-form/post-form.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialStylingComponent } from './components/material-styling/material-styling.component';

@NgModule({
  declarations: [// components go
    AppComponent,
    UsersComponent,
    VipComponent,
    PostsComponent,
    PostFormComponent,
    HomeComponent,
    NavbarComponent,
    MaterialStylingComponent
  ],
  imports: [ // modules go here
    // other imports ...
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [
    DataService,
    PostsService
  ], // services go here


  bootstrap: [AppComponent]
})
export class AppModule { }










