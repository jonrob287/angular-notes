import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UsersComponent } from './components/users/users.component';
import { PostsComponent } from './components/posts/posts.component';
import { TravelComponent } from "./components/travel/travel.component";
import { VipComponent } from "./components/vip/vip.component";


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path:'users',
    component: UsersComponent
  },
  {
    path: 'posts',
    component: PostsComponent
  },
  {
    path: 'vip',
    component: VipComponent
  },
  {
    path: 'travel',
    component: TravelComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
