import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import {Observable } from 'rxjs';
import {Post} from "../models/Post";
import {get} from "http";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})



export class PostsService {
  //PROPERTIES
  // set a url as a property
  postsURL: string = "https://jsonplaceholder.typicode.com/posts";


  //inject the httpclient as a dependency
  constructor(private http: HttpClient) {
  }//END OF THE CONSTRUCTOR

  //create a method, that will make a GET request
  getPosts(): Observable<Post[]>{
    return this.http.get<Post[]>(this.postsURL)
  }

  //create a method savePost()
  savePost(post: Post) : Observable<Post> {
    return this.http.post<Post>(this.postsURL, post, httpOptions)
  }

  //Create a method to Update Post
  updatePost(post: Post) :Observable<Post>{
    const url = `${this.postsURL}/${post.id}`     //url https://jsonplaceholder.typicode.com/posts/id
    return this.http.put<Post>(url, post, httpOptions)
  }

  //Create a method to Remove Post
  removePost(post: Post | number ) : Observable<Post> {
    const id = typeof post === 'number' ? post : post.id;
    const url = `${this.postsURL}/${id}`;  //url https://jsonplaceholder.typicode.com/id
    console.log('Deleting post');
    alert('Post removed');
    return this.http.delete<Post>(url, httpOptions);
  }


}

//END OF THE CLASS
