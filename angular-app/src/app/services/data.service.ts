import { Injectable } from '@angular/core';
import {User} from "../models/User";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class DataService {
  // Initialize a user's property and assign it to an array of User
  users: User[];
  data: Observable<any>



  constructor() {
    this.users = [
      {
        firstName: 'Bruce',
        lastName: 'Wayne',
        age: 30,
        email: 'brucewayne@email.com',
        image: '/assets/img/batman.png',
        balance:1000000,
        memberSince:new Date('03/01/1937 08:30:00'),
        isActive: true,
        hide: false,
        hideImage:false
      },
      {
        firstName: 'Diana',
        lastName: 'Prince',
        age: 110,
        email:'diana.prince@email.com',
        image: '/assets/img/wonderwoman.png',
        balance:1500000,
        memberSince:new Date('12/12/1942 12:45:00'),
        isActive: true,
        showUserForm: false,
      },
    ];// END OF OUR this.users ARRAY
  } //END OF CONSTRUCTOR

  // EXAMPLE OF OBSERVABLE
  // getData(){
  //   this.data = new Observable(observer => {
  //     setTimeout(() => {
  //       observer.next(1)
  //     },1000);
  //     setTimeout(() => {
  //       observer.next(2)
  //     },2000);
  //     setTimeout(() => {
  //       observer.next(3)
  //     },3000);
  //     setTimeout(() => {
  //       observer.next(4)
  //     },4000);
  //   });
  //   return this.data;
  // }

  // refactor our method to return an array as an observable
  //getUsers(), return a type of User[]
  getUsers():Observable<User[]> {
    console.log('fetching users from service')
    return of(this.users);
  }


  addUser(user: User){
    console.log('Added user from service')
    this.users.unshift(user);
  }

}//END OF CLASS

















