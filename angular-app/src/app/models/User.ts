export interface User {
  firstName: string,
  lastName: string,
  age?: number,
  // address: {
  //   street: string;
  //   city: string;
  //   state: string;
  // }
  email: string,
  image?: string,
  isActive?: boolean,
  balance?: number,
  memberSince?: any,
  hide?: boolean,
  showUserForm?: boolean
  hideImage?: boolean
}



