import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeboundComponent } from './codebound.component';

describe('CodeboundComponent', () => {
  let component: CodeboundComponent;
  let fixture: ComponentFixture<CodeboundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeboundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeboundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
