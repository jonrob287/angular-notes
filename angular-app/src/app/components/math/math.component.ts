import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
    num1:number;
    num2:number;
  constructor() {
                          // this.num1 = 50;
                          // this.num2 = 2;
                          // this.sumOf()
                          // console.log(this.sumOf(50,2))
    // this.sumOf(50,2)
    // this.differenceOf(50,2)
    // this.productOf(50,2)
    // this.quotientOf(50,2)
                          // console.log( +this.num1 + ' plus ' + this.num2 + ' is ' + this.sumOf()+'.')
                          // this.differenceOf()
                          // console.log( +this.num1 + ' minus ' + this.num2 + ' is ' + this.differenceOf()+'.')
                          // this.productOf()
                          // console.log( +this.num1 + ' multiplied by ' + this.num2 + ' is ' + this.productOf()+'.')
                          // this.quotientOf()
                          // console.log( +this.num1 + ' divided by ' + this.num2 + ' is ' + this.quotientOf()+'.')
    this.table()
  }
  ngOnInit(): void {
  }
  sumOf (x,y){
    this.num1 = x
    this.num2 = y
    // return this.num1 + this.num2
    return `Addition: ${this.num1} + ${this.num2} = ${this.num1 + this.num2}`
    // console.log(this.num1 + this.num2);

  }
  differenceOf(a,b){
    this.num1 = a
    this.num2 = b
    return `Subtraction: ${this.num1} - ${this.num2} = ${this.num1 - this.num2}`
    // console.log(this.num1 - this.num2)
  }
  productOf(x,y){
    this.num1 = x
    this.num2 = y
    return `Multiplication: ${this.num1} x ${this.num2} = ${this.num1 * this.num2}`
    // console.log(this.num1 * this.num2)
  }
  quotientOf(x,y){
    this.num1 = x
    this.num2 = y
    return `Division: ${this.num1} / ${this.num2} = ${this.num1 / this.num2}`
    // console.log(this.num1 / this.num2)
  }
  table(){
    for(var i = 1; i <= 100; i ++) {
      if (i % 3 === 0 && i % 15 !== 0) {
        console.log('Fizz');
      } else if (i % 5 === 0 && i % 15 !== 0) {
        console.log('Buzz');
      } else if (i % 15 === 0) {
        console.log('FizzBuzz')
      } else {
        console.log(i);
      }
    }
  }
}


// Create a new component named 'math' in your components directory
// In your math.component.ts, create two properties, 'num1' and 'num2'.
//   Set both properties as a number.
//   Next, create a method that takes in the two properties as inputs and returns the sum of the two properties.
//   Console log the result
// Create a method that takes in the two properties and returns the difference of the two numbers.
//   Console log the result
// Create a method that takes in the two properties and returns the product of the two numbers.
//   Console log the result
// Create a method that takes in the two properties and returns the quotient
// of the two numbers. Console log the result
// BONUS: Write a program that prints the numbers from 1 to 100. But for multiples of three,
//   print 'Fizz' instead of the number. For the multiples of five point, print 'Buzz'.
//   And for the numbers which are multiples of both three and five, print 'FizzBuzz'



//NEW EXERCISE
// Using String Interpolation, render the sum in math.component.html
// Render the difference in math.component.html by using String Interpolation
// Render the product in math.component.html by using String Interpolation
// Render the quotient in math.component.html by using String Interpolation
// Create a new component named 'vip' in your components directory
// In vip.component.ts, create one property 'member' and assign it as a Member data type
//   Outside of your class, create an interface named 'Member' where it has the following properties and data types:
//   firstName: string
// lastName: string
// username: string
// memberNo: number
// Inside of your constructor, create a member
// Using String Interpolation, render the member's information in the html.
// Inside of your app directory, create a models folder (if you haven't already).
// Inside of the models folder, create a file named 'Member.ts'.
//   Take your interface from vip.component.ts and place inside of 'Member.ts'
// (remember to 'export' your interface)
