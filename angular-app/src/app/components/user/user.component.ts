import { Component } from "@angular/core";
import {hasConstructor} from "@angular/compiler-cli/ngcc/src/migrations/utils";
import {last} from "rxjs/operators";
import {User} from "../../models/User";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  //PROPERTIES - like an attribute of the component
  //USUALLY go at the TOP

//     firstName = 'Clark';
//     lastName  = 'Kent';
//     age = 65;


  //MORE PRACTICAL WAY OF CREATING PROPERTIES
  //SYNTAX: propertyName: dataType
  //   firstName:string;
  //   lastName: string;
  //   age: number;
  //   whatever: any;
  //   hasKids: boolean;
  // //DATA-TYPES
  // numberArray: number[]; //this MUST be an array of numbers
  // stringArray: string[]; //this MUST be an array of strings
  // mixArray: any[]; // this can be an array of anything but IT HAS TO BE AN ARRAY

  //MIGHT SEE THIS MORE OFTEN
  user: User;

// METHODS - a "function" inside of a class

  //CONSTRUCTOR - runs every time when our component is initialized / called
  //DIFFERENCE BETWEEN constructor(){} and ngOnInit
  // constructor dependicy injections
  //ngoninit is used for Ajax calls or services


  constructor() {
    // console.log('Hello from user component');
    //
    // this.greeting();
    // console.log(this.age);  //undefined
    // this.hasBirthday();
    // console.log(this.age); //31

    // this.firstName = 'Clark';
    // this.lastName = 'Kent';
    // this.age = 65;
    // this.hasKids = false;
    // this.greeting();
    // // this.numberArray = ['help', 13, true, 45] //notice it does not recognize a string or boolean
    // this.numberArray = [ 13, 99, 45];
    // // this.stringArray = [911, 'help', true] // notice it does not recognize numbers or boolean
    // this.stringArray = ['help', 'me', 'now'];
    // // this.mixArray = 767; // notice it will cause an error if NOT  in an array
    // this.mixArray = ['help', 24, true];

    //CONNECTING WITH OUR INTERFACE
    //   this. keyword to access properties

    this.user = {
      firstName: 'Jane',
      lastName: 'Doe',
      age: 40,
      email: 'jane.doe@gmail.com'
    }

  }

    greeting (){
      // console.log('Hello there, ' + this.firstName +' '+ this.lastName + '!');
      return `Hello there, ${this.user.firstName} ${this.user.lastName}!`
    }
  // hasBirthday(){
  //     this.age =30
  //   return this.age +=1;
  // }
} //END OF CLASS (DON'T DELETE)

//ALTERNATE WAY TO CREATE AN INTERFACE

// interface User {
//   firstName: string;
//   lastName: string;
//   age: number;
//   address: {
//     street: string;
//     city: string;
//     state: string;
//   }
// }

