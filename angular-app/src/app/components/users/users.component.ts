import { Component, OnInit, ViewChild } from '@angular/core';
import {User} from "../../models/User";
import {isAsciiLetter} from "codelyzer/angular/styles/chars";
import { DataService } from "../../services/data.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {
  user: User ={
    firstName: '',
    lastName:'',
    age: null,
    email:'',
  }

  //PROPERTIES
  users:User[];
  displayInfo: boolean = true;
  enableAddUser: boolean;
  currentClass:{}; // basically an empty object
  currentStyle:{};
  showUserForm: boolean = false;
  @ViewChild('userForm')form: any;

  //create a property for the observable
  data: any;




  //inject the service as a dependency in our constructor
  constructor(private dataService: DataService) { }


  ngOnInit(): void {
    // this.users = this.dataService.getUsers();
    //subscribe to the observable
    this.dataService.getUsers().subscribe(users => {
      this.users = users;
    })


    //subscribe to the observable
    // this.dataService.getData().subscribe(data => {
    //   console.log(data)
    // })
    this.enableAddUser = true;
    this.setCurrentClasses();
    this.setCurrentStyle();

    //CALLING OUR ADDUSER ()
    // this.addUser({
    //   firstName: 'Jason',
    //   lastName: 'Todd',
    //   age: 13,
    // });
  }// END OF NGONINIT() (DON'T DELETE)


  // Create a method that adds a new user to the array
  // addUser(user: User){
  //   this.users.unshift(this.user);
  // }
  //Create a method that will change the state of our button
  setCurrentClasses(){
    this.currentClass = {
      'btn-dark': this.enableAddUser
    }
  }
  setCurrentStyle(){
    this.currentStyle = {
      'padding-top': this.displayInfo ? '0' : '150px'
    }
  }
    // adding in fireEvent method under click event
  //Types of events
  //(mousedown) (mouseup) (mouseout) (click) (dblclick) (drag)

    fireEvent(e){
      // console.log('The drag event has occurred')
      console.log(e.type)
    }
  toggleHide(user: User){
    user.hide = !user.hide;
  }
toggleHideImage(user:User){
    user.hideImage = !user.hideImage;
}
onSubmit({value,valid}:{value:User, valid:boolean}){
    if(!valid){
      alert('Your Form is invalid!!!')
    }else {
      value.isActive = true;
      value.memberSince = new Date();
      value.hide = true;
    }
      // Faith's Code from Forms
      // this.users.unshift(value);
  // Stephen's code from services
  this.dataService.addUser(value);

      //resets the form
      this.form.reset();
  }






}// END OF CLASS DO NOT DELETE






