import { Component, OnInit } from '@angular/core';
import {Member} from "../../models/Member";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  // template: ` <h1>Hello</h1>`,
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {
  loading: boolean = false;
  member: Member[];

  constructor() {

  }

  ngOnInit(): void {
    setTimeout(() => this.loading = true, 5000);
    this.member = [
      {
        firstName: 'Jonathan',
        lastName: 'Robles',
        userName: 'Jonathan.Robles',
        memberNo: 287,
      },
      {
        firstName: 'Tiffany',
        lastName: 'Robles',
        userName: 'Tiffany.Robles',
        memberNo: 988,
      },
      {
        firstName: 'Sophie',
        lastName: 'Robles',
        userName: 'Sophie.Robles',
        memberNo: 811,
      },
      {
        firstName: 'Matthew',
        lastName: 'Robles',
        userName: 'Matthew.Robles',
        memberNo: 913,
      },
      {
        firstName: 'Tara',
        lastName: 'Robles',
        userName: 'Tara.Robles',
        memberNo: 402,
      }
    ]// END OF OUR ARRAY

  }// END OF NGONINIT() (DON'T DELETE)

} //END OF CLASS


// export interface Member {
//   firstName: string;
//   lastName: string;
//   userName: string;
//   memberNo: number;
// }


// Create a new component named 'vip' in your components directory
// In vip.component.ts, create one property 'member' and assign it as a Member data type
//   Outside of your class, create an interface named 'Member' where it has the following properties and data types:
//   firstName: string
// lastName: string
// username: string
// memberNo: number
// Inside of your constructor, create a member
// Using String Interpolation, render the member's information in the html.
// Inside of your app directory, create a models folder (if you haven't already).
// Inside of the models folder, create a file named 'Member.ts'.
//   Take your interface from vip.component.ts and place inside of 'Member.ts'
// (remember to 'export' your interface)
