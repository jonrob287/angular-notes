import { Component, OnInit } from '@angular/core';
import {Post} from "../../models/Post";
import {PostsService} from "../../services/posts.service";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  //PROPERTIES
  posts: Post[];

  currentPost: Post = {
    id: 0,
    title: '',
    body: '',
  };

  isEdit: boolean = false;

  //inject our service as a dependency
  constructor(private postService: PostsService) { }


  //fetch the posts when ngOnInit() is initialized
  ngOnInit(): void {
    //subscribe to our observable
    this.postService.getPosts().subscribe(posts => {
      console.log('Fetching posts');
      console.log(posts);
      this.posts = posts;
    })
  }

  onNewPost(post: Post) {
    this.posts.unshift(post);
  }

  editPost(post: Post){
    this.currentPost = post;
    //included for the update post button
    this.isEdit = true;
  }

  onUpdatePost(post: Post){
    this.posts.forEach((current, index) => {
      if (post.id === current.id) {

        //splicing the old post
        this.posts.splice(index, 1)

        //replacing the old post with the updated one,
        //AND putting the updated post to the top of the list.

        this.posts.unshift(post);
        //resetting our button to "submit Post" btn

        this.isEdit = false;
        // resetting our form

        this.currentPost={
          id:0,
          title:'',
          body:''
        };
      }
    })
  }

  //create a method that will delete a single post
  deletePost(post: Post){
    if (confirm('Are you sure?')) {
      this.postService.removePost(post.id).subscribe(() => {
        this.posts.forEach((current, index) => {
          if (post.id === current.id) {
            this.posts.slice(index, 1);
          }
        })
      })
    }
  }

}//End of class dont delete
