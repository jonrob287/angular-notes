          // Exercise #1
          // Create a new component name 'hello' in your components directory.
          //   Create your typescript file 'hello.component.ts'
          // In your ts file:
          //   -	make sure you import Component from @angular/core
          // -	include your Component metadata (@Component)
          // -	export your class

          import {Component} from "@angular/core";
          @Component({
            selector: 'app-hello',
            templateUrl: './hello.component.html',
            styleUrls: ['./hello.component.css']
          })
          export class HelloComponent {

          }

          // Create your html file 'hello.component.html'
          // In your html file:
          //   -	print out the message 'Hello World' onto your webpage

          // Create your html file 'hello.component.css'
          // In you css file:
          //   -	target the element containing your content and change the background color to red, text color to yellow, and underline your content.



          //   Exercise #2
          // In your components directory, create a new folder name 'travel'.
          //   In your new folder, create an your html, css, and typescript file.
          //   In your html file, include a title such as, 'My Travel Blog' or 'Stephen's Traveling', and an unordered list containing 3 places/cities/countries etc., that you've been to.
          //   In your css file, the background should not be white – change the background color.
          // *Import, create a component/decorator, and create/export a class








