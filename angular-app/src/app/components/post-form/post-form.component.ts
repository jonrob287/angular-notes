import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { PostsService } from "../../services/posts.service";
import { Post } from "../../models/Post";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  // PROPERTIES
  //defining our Output as 'newPost' and setting it to an EventEmitter, which will have the type of Post and assigning it as a new EvenEmitter.
  @Output() newPost: EventEmitter<Post> = new EventEmitter();
  @Output() updatedPost: EventEmitter<Post> = new EventEmitter();
  //defined our Input as a property 'currentPost' and set it as a Post data-type
  @Input() currentPost: Post;
  @Input() isEdit: boolean;


  //injection service as a dependency
  constructor(private postService: PostsService) { }

  ngOnInit(): void {}

  addPost(title,body){
    if (!title || !body){
      alert('Please enter a post')
    }else{
      this.postService.savePost({title, body} as Post).subscribe(post => {
        // console.log(post)
        this.newPost.emit(post);
        }
      )
    }
  }

  updatePost(){
    // console.log("updating post....");
    this.postService.updatePost(this.currentPost).subscribe(post => {
      console.log(post);
      this.isEdit = false;
      this.updatedPost.emit(post)
    })
  }

}




