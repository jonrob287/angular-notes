// import { Component } from '@angular/core';
//
//
// @Component({
//   selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.css']
// })
// export class AppComponent {
//   title = 'Bravo Angular App';
// }
//SIMPLE FORMS EXAMPLE
// import {Component} from '@angular/core';
// @Component({
//   selector: 'app-root',
//   //EXTERNAL TEMPLATE
//   templateUrl: './app.component.html',
//   //INLINE TEMPLATE
//   template:`<!--<form #corona="ngForm" (ngSubmit) = "onclickSubmit(corona.value)">-->
// <!--    <input type = "text" name = "patient_name" placeholder = "name" ngModel>-->
// <!--    <br/>-->
// <!--    <input type = "text" name = "patient_age" placeholder = "age" ngModel>-->
// <!--    <br/>-->
// <!--    <input type = "submit" value = "submit">-->
// <!--  </form>-->
//   `,
//   styleUrls: ['./app.component.css']
// })
// export class AppComponent {
// onclickSubmit(formData){
//   console.log('Corona infected patient is: ' + formData.patient_name);
//   console.log('Corona infected patient is: ' +formData.patient_age);
// }
// }
//
//REACTIVE FORMS EXAMPLE

//app.component.ts

                //Not working
                // import {Component} from '@angular/core';
                // import {FormGroup, FormControl} from "@angular/forms";
                //
                // @Component({
                //   selector: 'app-root',
                //   templateUrl:'./app.component.html',
                //   styleUrls:['./app.component.css']
                // })
                // export class AppComponent implements OnInit {
                //   form: FormGroup;
                //   ngOnInit(){
                //     this.form=new FormGroup({
                //       patient_name: new FormControl(''),
                //       patient_age: new FormControl(''),
                //     });
                //   }
                // onClickSubmit(formData)
                // {
                //   console.log('Corona infected patient is: ' + formData.patient_name);
                //   console.log('Corona infected patient is:' + formData.patient_age);
                // }
                // }

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  form: FormGroup;
  ngOnInit() {
    this.form = new FormGroup({
      patient_name: new FormControl(''),
      patient_age: new FormControl(''),
    });
  }
  onClickSubmit(formData) {
    console.log('Corona Infected Patient name is: ' + formData.patient_name);
    console.log('Corona Infected Patient age is: ' + formData.patient_age);
  }
}
